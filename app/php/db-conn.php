<?php
$isDev = false;

// Массив для проверки localhost
$whitelist = array('127.0.0.1', '::1');

// Проверка сайта на localhost 
if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
    // Если же это localhost - ставим переменную isDev в true
    $isDev = true;
}

if ($isDev) {
    $servername = "localhost";
    $database = "process";
    $username = "root";
    $password = "root";
} else {
    $servername = "localhost";
    $database = "adnfopzh_process";
    $username = "adnfopzh";
    $password = "[ojP6Ff:3P1Zl6";
}

// Устанавливаем соединение
$conn = mysqli_connect($servername, $username, $password, $database);
// Проверяем соединение
if (!$conn) {
   die("Connection failed: " . mysqli_connect_error());
}
