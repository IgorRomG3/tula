<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'bootstrap.php';
require 'db-conn.php';

// Creating the new document...
$phpWord = new \PhpOffice\PhpWord\PhpWord();

// echo $_POST['fio'];

$phpWord->addParagraphStyle(
    'multipleTab',
    array(
        'tabs' => array(
            new \PhpOffice\PhpWord\Style\Tab('left', 1550),
            new \PhpOffice\PhpWord\Style\Tab('center', 3200),
            new \PhpOffice\PhpWord\Style\Tab('right', 5300),
        )
    )
);

$phpWord->addParagraphStyle(
    'rightTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9000)))
);

$phpWord->addParagraphStyle(
    'centerTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680)))
);

$docName = "Б.3 Шаблон формы «Акт по факту непредставления (предоставления не в полном объеме или недостоверных) сведений по запросу СП ТО»";
$auditorFio = $_POST['auditor-fio'];
$city = $_POST['city'];
$dateDay = $_POST['date-day'];
$dateMonth = $_POST['date-month'];
$dateToDay = $_POST['date-to-day'];
$dateToMonth = $_POST['date-to-month'];
$dateToYear = $_POST['date-to-year'];
$dateYear = $_POST['date-year'];
$event = $_POST['event'];
$notProvide = $_POST['not-provide'];
$objectEvent = $_POST['object-event'];
$pointYearPlan = $_POST['point-year-plan'];
$positionFio = $_POST['position-fio'];
$questions = $_POST['questions'];
$targetFio = $_POST['target-fio'];

$hintFontStyling = array('name' => 'Times New Roman', 'align' => 'right', 'italic' => true);
$centerAlign = array('align'=>'center', 'spaceAfter' => 100);
$rightAlign = array('align'=>'end', 'spaceAfter' => 100);
$leftAlign = array('align' => 'start', 'indentation' => array('left' => 0));
$hintLeftParagraphStyling = array('align'=>'start', 'spaceAfter' => 100);
$regularFont = array('name' => 'Times New Roman', 'size' => 10);
$regularBoldFont = array('name' => 'Times New Roman', 'size' => 10, 'bold' => true);

/* Note: any element you append to a document must reside inside of a Section. */


// Adding an empty Section to the document...
$section = $phpWord->addSection(array(
    // 'orientation' => 'landscape',
    'marginTop' => '200',
    'marginRight' => '1500',
    'marginLeft' => '1500'
));
 
// Adding Text element with font customized inline...
$section->addText(
    $docName,
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right', 'bold' => true),
    array_merge($rightAlign, array('spaceAfter' => 500))
);

$section->addText(
    'А К Т',
    $regularBoldFont,
    array_merge($centerAlign)
);

$section->addText(
    'по факту непредставления (предоставления не в полном объеме или недостоверных) сведений по запросу счетной палаты Тульской области',
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right', 'bold' => true),
    array_merge($centerAlign, array('indentation' => array('left' => 1000, 'right' => 1000)))
);

$section->addLine(
    array('weight' => 1, 'width' => 450, 'height' => 0, 'color' => 000000)
);

$table = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));

$tableRow = $table->addRow(500);

$tableCellLeft = $table->addCell(3000);
$tableCellCenter = $table->addCell(3000);
$tableCellRight = $table->addCell(3000);

$tableCellLeft->addText(
    $city,
    $regularFont,
    array_merge($leftAlign)
);

$tableCellLeft->addText(
    '(город, иной населенный пункт)',
    $hintFontStyling,
    array_merge($leftAlign)
);

$tableCellRight->addText(
    '«' . $dateDay . '» ' . $dateMonth . ' 20' . $dateYear . 'г.',
    $regularFont,
    array_merge($rightAlign)
);

$section->addText(
    'В соответствии с ' . $pointYearPlan,
    $regularFont,
    array_merge($leftAlign, array('spaceBefore' => 200))
);

$section->addText(
    '(пункт годового плана работы счетной палаты Тульской области на соответствующий финансовый год, иные основания для проведения контрольного мероприятия, предусмотренные в Законе Тульской области «О счетной палате Тульской области»)',
    $hintFontStyling,
    array_merge($leftAlign, array('indentation' => array('left' => 1400)))
);

$section->addText(
    'В ' . $objectEvent,
    $regularFont,
    array_merge($leftAlign, array('spaceBefore' => 200))
);

$section->addText(
    '(наименование объекта контрольного мероприятия)',
    $hintFontStyling,
    array_merge($leftAlign, array('indentation' => array('left' => 250)))
);

$section->addText(
    'проводится подготовка к контрольному мероприятию ' . $event,
    $regularFont,
    array_merge($leftAlign, array('spaceBefore' => 200))
);

$section->addText(
    '(наименование контрольного мероприятия)',
    $hintFontStyling,
    array_merge($leftAlign, array('indentation' => array('left' => 4700), 'spaceAfter' => 200))
);

$table = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));

$tableRow = $table->addRow(500);

$tableCellLeft = $table->addCell(4450);
$tableCellCenter = $table->addCell(100);
$tableCellRight = $table->addCell(4450);

$tableCellLeft->addText(
    'В соответствии со пунктом 3 части 1 статьи 14 и статьи 15 Закона Тульской области от 04.12.2008 № 1147-ЗТО «О счетной палате Тульской области» были запрошены документы (информация) по следующим вопросам: ',
    $regularFont,
    array_merge($leftAlign)
);

$tableCellRight->addText(
    $questions,
    $regularFont,
    array_merge($leftAlign)
);

$section->addText(
    'Срок предоставления информации истёк: «' . $dateToDay . '» ' . $dateToMonth . ' 20' . $dateToYear . 'г.',
    $regularFont,
    array_merge($leftAlign, array('spaceBefore' => 200, 'spaceAfter' => 200))
);

$table = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));

$tableRow = $table->addRow(500);

$tableCellLeft = $table->addCell(4450);
$tableCellCenter = $table->addCell(100);
$tableCellRight = $table->addCell(4450);

$tableCellLeft->addText(
    'В нарушении статьи 15 Закона Тульской области от 04.12.2008 № 1147-ЗТО «О счетной палате Тульской области» к настоящему времени счетной палате Тульской области информация',
    $regularFont,
    array_merge($leftAlign)
);

$tableCellRight->addText(
    $notProvide,
    $regularFont,
    array_merge($leftAlign, array('spaceAfter' => 150))
);

$tableCellRight->addText(
    '(не представлена, представлена не в полном объеме, представлена недостоверная)',
    $hintFontStyling,
    array_merge($leftAlign)
);

$section->addText(
    'что влечет за собой ответственность, установленную законодательством Российской Федерации.',
    $regularFont,
    array_merge($leftAlign, array('spaceBefore' => 250, 'spaceAfter' => 250))
);

$table = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));

$tableRow = $table->addRow(500);

$tableCellLeft = $table->addCell(4450);
$tableCellCenter = $table->addCell(100);
$tableCellRight = $table->addCell(4450);

$tableCellLeft->addText(
    'Настоящий Акт составлен в двух экземплярах, один из которых вручен (направлен) для ознакомления',
    $regularFont,
    array_merge($leftAlign)
);

$tableCellRight->addText(
    $positionFio,
    $regularFont,
    array_merge($leftAlign, array('spaceAfter' => 100))
);

$tableCellRight->addText(
    '(должностное лицо проверяемого объекта, Ф.И.О.)',
    $hintFontStyling,
    array_merge($leftAlign)
);

$tableBottom = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc', 'spaceAfter' => 300));

$tableBottomRow = $tableBottom->addRow(300);

$tableBottomCellLeft = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCell = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCellRight = $tableBottom->addCell(3000, array('valign' => 'bottom'));

$tableBottomCellLeft->addText(
    'Аудитор счетной палаты Тульской области',
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    null,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    $targetFio,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

$tableBottomRowSec = $tableBottom->addRow(300);

$tableBottomCellLeft = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCell = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCellRight = $tableBottom->addCell(3000, array('valign' => 'bottom'));

$tableBottomCellLeft->addText(
    'Должность',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    'Личная подпись',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    'ФИО',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

$tableBottomRowSec = $tableBottom->addRow(300);

$tableBottomCellLeft = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCell = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCellRight = $tableBottom->addCell(3000, array('valign' => 'bottom'));

$tableBottomCellLeft->addText(
    'Один экземпляр акта получил:',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start', 'spaceAfter' => 100))
);

$tableBottomCellLeft->addText(
    null,
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    null,
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    $auditorFio,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

$tableBottomRowSec = $tableBottom->addRow(300);

$tableBottomCellLeft = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCell = $tableBottom->addCell(3000, array('valign' => 'bottom'));
$tableBottomCellRight = $tableBottom->addCell(3000, array('valign' => 'bottom'));

$tableBottomCellLeft->addText(
    'Должность',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    'Личная подпись',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    'ФИО',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

// Saving the document as OOXML file...
$returnArray = array();
$returnArray['docName'] = $docName;
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$fullDocumentPath = '../' . 'documents/' . $returnArray['docName'] . '.doc';
$objWriter->save($fullDocumentPath);

$sql = "INSERT INTO notProvideAct (auditorFio, city, dateDay, dateMonth, dateToDay, dateToMonth, dateToYear, dateYear, event, notProvide, objectEvent, pointYearPlan, positionFio, questions, targetFio)
VALUES ('$auditorFio', '$city', '$dateDay', '$dateMonth', '$dateToDay', '$dateToMonth', '$dateToYear', '$dateYear', '$event', '$notProvide', '$objectEvent', '$pointYearPlan','$positionFio', '$questions', '$targetFio')";
if(mysqli_query($conn, $sql)){
    $returnArray['db'] = "Запись в базу данных произведена.";
}else{
    $returnArray['db'] = "Ошибка записи в базу данных.";
}

mysqli_close($conn);

echo json_encode($returnArray, JSON_UNESCAPED_UNICODE);