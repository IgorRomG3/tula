<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'bootstrap.php';
require 'db-conn.php';

// Creating the new document...
$phpWord = new \PhpOffice\PhpWord\PhpWord();

$phpWord->addParagraphStyle(
    'multipleTab',
    array(
        'tabs' => array(
            new \PhpOffice\PhpWord\Style\Tab('left', 1550),
            new \PhpOffice\PhpWord\Style\Tab('center', 3200),
            new \PhpOffice\PhpWord\Style\Tab('right', 5300),
        )
    )
);

$phpWord->addParagraphStyle(
    'rightTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9000)))
);

$phpWord->addParagraphStyle(
    'centerTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680)))
);

$docName = "Б.2 Шаблон формы «Оформление запроса СП ТО о предоставлении информации»";
$targetDoc = $_POST['target-name'];
$year = $_POST['year-ready'];
$point = $_POST['point-ready'];
$directorName = $_POST['to-whom'];
$directorPosition = $_POST['position'];
$event = $_POST['event-name'];
$toDateMonth = $_POST['to-month'];
$toDateYear = $_POST['to-year'];
$questionArray = $_POST['documents-array'];
$docView = $_POST['documents-view'];
$fio = $_POST['fio'];
$titleForm = $_POST['title-input-one'];
$numberForm = $_POST['title-input-sec'];
$titleFormSec = $_POST['title-input-third'];
$numberFormSec = $_POST['title-input-fourth'];
$positionSelect = $_POST['position-select'];

$hintFontStyling = array('name' => 'Times New Roman', 'align' => 'right', 'italic' => true);
$rightAlign = array('align' => 'end', 'spaceAfter' => 100);
$leftAlign = array('align' => 'start', 'indentation' => array('left' => 0));
$hintLeftParagraphStyling = array('align' => 'start', 'spaceAfter' => 100);
$regularFont = array('name' => 'Times New Roman', 'size' => 10);
$regularBoldFont = array('name' => 'Times New Roman', 'size' => 10, 'bold' => true);

/* Note: any element you append to a document must reside inside of a Section. */


// Adding an empty Section to the document...
$section = $phpWord->addSection(array(
    // 'orientation' => 'landscape',
    'marginTop' => '200',
    'marginRight' => '1500',
    'marginLeft' => '1500'
));
/*
 * Note: it's possible to customize font style of the Text element you add in three ways:
 * - inline;
 * - using named font style (new font style object will be implicitly created);
 * - using explicitly created font style object.
 */


// Adding Text element with font customized inline...
$section->addText(
    $docName,
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right', 'bold' => true),
    array_merge($rightAlign)
);


$table = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));
$table->addRow(1000);
$cellLeft = $table->addCell(3500);
$cell = $table->addCell(1000);
$cellRight = $table->addCell(4500);

$documentRoot = $_SERVER['DOCUMENT_ROOT'];

if ($isDev) {
    $imagePath = "/images/tula.png";
}else{
    $imagePath = "/process/images/tula.png";
}

$cellLeft->addImage($documentRoot . $imagePath, array('align' => 'center', 'width' => 50));
$cellLeft->addText(
    'СЧЕТНАЯ ПАЛАТА',
    array('name' => 'Times New Roman', 'size' => 8, 'bold' => true),
    array_merge(array('spaceBefore' => 100, 'align' => 'center'))
);
$cellLeft->addText(
    'ТУЛЬСКОЙ ОБЛАСТИ',
    array('name' => 'Times New Roman', 'size' => 8, 'bold' => true),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellLeft->addText(
    'просп. Ленина, д.2, г. Тула, 300041',
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 100, 'align' => 'center'))
);

$cellLeft->addText(
    'тел. 8 (4872) 55-66-43, факс 8 (4872) 55-66-96',
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellLeft->addText(
    'E-mail: sptulobl@tularegion.ru',
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellLeft->addText(
    'www.sptulobl.ru',
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellLeft->addText(
    $titleForm . ' № ' . $numberForm,
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellLeft->addText(
    'На № ' . $titleFormSec . ' от ' . $numberFormSec,
    array('name' => 'Times New Roman', 'size' => 8),
    array_merge(array('spaceBefore' => 10, 'align' => 'center'))
);

$cellRight->addText(
    $directorName,
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right'),
    array_merge(array('spaceBefore' => 700, 'align' => 'center'))
);

$cellRight->addText(
    'Руководителю государственного органа (организации, учреждения)',
    $hintFontStyling,
    array('align' => 'center')
);

$cellRight->addText(
    $directorPosition,
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right'),
    array_merge(array('spaceBefore' => 700, 'align' => 'center'))
);

$cellRight->addText(
    '(должность, Ф.И.О.)',
    $hintFontStyling,
    array('align' => 'center')
);

$section->addText(
    'Уважаемый ' . $targetDoc . '!',
    array('name' => 'Times New Roman', 'size' => 10, 'align' => 'right', 'bold' => true),
    array('align' => 'center', 'spaceAfter' => 450)
);

$section->addText(
    'В соответствии со статьей 13 Закона Тульской области от 04.12.2008 № 1147-ЗТО «О счетной палате Тульской области», на основании плана работы счетной палаты Тульской области на 20' . $year . ' год (пункт ' . $point . ') проводится подготовка к контрольному мероприятию',
    $regularFont,
    array_merge($leftAlign, array('spaceAfter' => 450))
);

$section->addText(
    $event,
    $regularFont,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceBefore' => 450, 'spaceAfter' => 50))
);

$section->addText(
    '(наименование контрольного мероприятия)',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceAfter' => 450))
);

$section->addText(
    'Учитывая изложенное, в рамках подготовки к контрольному мероприятию, прошу Вас в срок до ' . $toDateMonth . ' 20' . $toDateYear . ' предоставить в счетную палату Тульской области',
    $regularFont,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceAfter' => 100))
);

$section->addText(
    $questionArray,
    $regularFont,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceBefore' => 250, 'spaceAfter' => 50))
);

$section->addText(
    '(перечень вопросов (документов, информации) для предоставления, в т.ч. формы таблиц, необходимые для заполнения)',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceAfter' => 250))
);

$section->addText(
    'Информацию необходимо представить ' . $docView,
    $regularFont,
    array_merge($hintLeftParagraphStyling, $leftAlign, array('spaceAfter' => 0))
);
$section->addText(
    '(указывается в каком виде должна быть представлена информация)',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('spaceAfter' => 550))
);

$tableBottom = $section->addTable(array('borderColor' => 'ffffff', 'borderSize'  => 6, 'cellMargin'  => 50, 'layout' => 'fixed', 'bgColor' => 'cccccc'));

$tableBottomRow = $tableBottom->addRow(500);

$tableBottomCellLeft = $tableBottom->addCell(3000);
$tableBottomCell = $tableBottom->addCell(3000);
$tableBottomCellRight = $tableBottom->addCell(3000);

$tableBottomCellLeft->addText(
    $positionSelect,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    null,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    $fio,
    $regularBoldFont,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

$tableBottomRowSec = $tableBottom->addRow(500);

$tableBottomCellLeft = $tableBottom->addCell(3000);
$tableBottomCell = $tableBottom->addCell(3000);
$tableBottomCellRight = $tableBottom->addCell(3000);

$tableBottomCellLeft->addText(
    'Должность',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'start'))
);

$tableBottomCell->addText(
    'Личная подпись',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'center'))
);

$tableBottomCellRight->addText(
    'ФИО',
    $hintFontStyling,
    array_merge($hintLeftParagraphStyling, $rightAlign, array('align' => 'end'))
);

// Saving the document as OOXML file...
$returnArray = array();
$returnArray['docName'] = $docName;
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$fullDocumentPath = '../' . 'documents/' . $returnArray['docName'] . '.doc';
$objWriter->save($fullDocumentPath);

$sql = "INSERT INTO SPTORequest (targetDoc, directorName, pointReady, yearReady, directorPosition, event, toDateYear, toDateMonth, numberFormSec, titleFormSec, numberForm, titleForm, fio, docView, questionArray, positionSelect)
VALUES ('$targetDoc', '$directorName', '$point', '$year', '$directorPosition', '$event', '$toDateYear', '$toDateMonth', '$numberFormSec', '$titleFormSec', '$numberForm', '$titleForm','$fio', '$docView', '$questionArray', '$positionSelect')";
if(mysqli_query($conn, $sql)){
    $returnArray['db'] = "Запись в базу данных произведена.";
}else{
    $returnArray['db'] = "Ошибка записи в базу данных.";
}
mysqli_close($conn);

echo json_encode($returnArray, JSON_UNESCAPED_UNICODE);
