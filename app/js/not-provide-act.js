$(document).ready(function () {
    submitForm();
});

function submitForm() {
    $('#btn-submit').click(function (e) {
        e.preventDefault();
        console.log(serializeToObject($('#main-form')));
        ajaxRequest(serializeToObject($('#main-form')), 'php/not-provide-controller.php');
    });
}

function ajaxRequest(data, url) {
    console.log(data, url);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        success: function (response) { //Данные отправлены успешно
            let responseArray = JSON.parse(response);
            console.log(responseArray);
            responseOnPage(responseArray.db);
            cross_download("documents/" + responseArray.docName + ".doc", responseArray.docName + ".doc");
        },
        error: function (response) { // Данные не отправлены
            console.log('Ошибка отправки формы,' + response);
        }
    });
}

function responseOnPage(responseDb) {
    $('.response-text').text(responseDb);
    $('.response-page').fadeIn(300).delay(1500).fadeOut(300);
}

function serializeToObject(target) {
    var formdata = target.serializeArray();
    var data = {};
    $(formdata).each(function (index, obj) {
        data[obj.name] = obj.value;
    });
    return data;
}

function cross_download(url, fileName) {
    var req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.responseType = "blob";

    var __fileName = fileName;

    req.onload = function () {
        var blob = req.response;
        var contentType = req.getResponseHeader("content-type");

        if (window.navigator.msSaveOrOpenBlob) {
            // Internet Explorer
            window.navigator.msSaveOrOpenBlob(new Blob([blob], {type: contentType}), fileName);
        } else {

            var link = document.createElement('a');
            document.body.appendChild(link);
            link.download = __fileName;
            link.href = window.URL.createObjectURL(blob);
            link.click();
            document.body.removeChild(link); //remove the link when done
        }
    };
    req.send();
}